const buttonLight = document.querySelectorAll('.btn');
let prevButtonText = '';
document.addEventListener('keydown', (event)=>{
    for (let elem of buttonLight){
        elem.style.color = ''

        if (elem.innerText === prevButtonText) {
            elem.style.color = 'black';
        }
        if (event.code ===`Key${elem.innerText}` || event.code === elem.innerText ) {
            elem.style.color = 'blue';
            prevButtonText = elem.innerText;
        }
    }

});